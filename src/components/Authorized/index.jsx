import React, { PureComponent } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { roles } from '../../configs';

class AuthorizedRoute extends PureComponent {
  render() {
    const {
      isLoggedIn,
      role,
      component: Component,
      authority,
      acceptedRoles,
      render,
      ...rest
    } = this.props;
    return (
      <Route
        {...rest}
        render={(props) => {
          if (authority === true) {
            // If component require authorization

            if (isLoggedIn === true) {
              // If user logged in

              if (acceptedRoles.includes(role)) {
                // Check user's role
                return <Component {...props} />;
              }

              // Redirect if role is mismatch
              if (role === roles.ROLE_STUDENT) {
                return <Redirect to={{ pathname: '/' }} />;
              }
              return <Redirect to={{ pathname: '/admin' }} />;
            }

            // If user didn't log in
            if (acceptedRoles.includes(roles.ROLE_STUDENT)) {
              return <Redirect to={{ pathname: '/login' }} />;
            }
            return <Redirect to={{ pathname: '/admin/login' }} />;
          }

          // If component don't require authorization
          return <Component {...props} />;
        }}
      />
    );
  }
}

AuthorizedRoute.propTypes = {
  exact: PropTypes.bool,
  authority: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  role: PropTypes.string,
  acceptedRoles: PropTypes.arrayOf(PropTypes.string),
  path: PropTypes.string.isRequired,
  component: PropTypes.func.isRequired,
};
AuthorizedRoute.defaultProps = {
  acceptedRoles: [],
  exact: false,
  role: null,
};

const mapStateToProps = state => ({
  isLoggedIn: state.auth.data.isLoggedIn,
  role: state.auth.data.role,
});
export default withRouter(connect(mapStateToProps)(AuthorizedRoute));
