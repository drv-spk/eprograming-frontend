import React, { Component } from 'react';
import { Icon, Menu, Sidebar } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { sidebarItems } from '../../configs';
import './index.css';

const SidebarItems = props =>
  props.items.map((item, index) => (
    <Menu.Item key={index} as="a" className="menu-item">
      <Icon name={item.icon} />
      {item.title}
    </Menu.Item>
  ));

class AdminSideBar extends Component {
  render() {
    const { showSideBar = true } = this.props;

    return (
      <Sidebar
        as={Menu}
        animation="overlay"
        inverted
        onHide={this.handleSidebarHide}
        vertical
        visible={showSideBar}
        width="thin"
      >
        <SidebarItems items={sidebarItems} />
      </Sidebar>
    );
  }
}

AdminSideBar.propTypes = {
  showSideBar: PropTypes.bool,
};

AdminSideBar.defaultProps = {
  showSideBar: true,
};

export default AdminSideBar;
