import { authentication } from './types';
import action from '../utils/action';

export const loginRequest = action(authentication.LOGIN_REQUEST);
export const loginSuccess = action(authentication.LOGIN_SUCCESS);
export const logout = action(authentication.LOGOUT);
