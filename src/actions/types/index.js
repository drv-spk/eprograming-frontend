export const authentication = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  SET_USER_INFO_SUCCESS: 'SET_USER_INFO_SUCCESS',
  LOGOUT: 'LOGOUT',
};

export const pingpong = {
  PING: 'PING',
  PONG: 'PONG',
};
