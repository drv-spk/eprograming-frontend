import { pingpong } from './types';
import { action } from '../utils/action';

export const ping = action(pingpong.PING);

export const pong = action(pingpong.PONG);
