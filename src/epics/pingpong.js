import { ofType } from 'redux-observable';
import { mapTo, delay } from 'rxjs/operators';
import { pingpong } from '../actions/types';

export default action$ =>
  action$.pipe(
    ofType(pingpong.PING),
    delay(1000), // Asynchronously wait 1000ms then continue
    mapTo({ type: pingpong.PONG }),
  );
