import React, { createElement } from 'react';
import { Button } from 'semantic-ui-react';
import config from './config';
import './index.css';

const Exception = ({
  linkElement = 'a', type, title, desc, img, actions, ...rest
}) => {
  const pageType = type in config ? type : '404';
  return (
    <div className="exception" {...rest}>
      <div className="imgBlock">
        <div
          className="imgEle"
          style={{ backgroundImage: `url(${img || config[pageType].img})` }}
        />
      </div>
      <div className="content">
        <h1>{title || config[pageType].title}</h1>
        <div className="desc">{desc || config[pageType].desc}</div>
        <div className="actions">
          {actions
            || createElement(
              linkElement,
              {
                to: '/',
                href: '/',
              },
              <Button primary>Back</Button>,
            )}
        </div>
      </div>
    </div>
  );
};

export default Exception;
