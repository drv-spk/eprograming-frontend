import React, { Component } from 'react';
import {
  Icon,
  Menu,
  Sidebar,
  Dropdown,
  Table,
  Grid,
  Segment,
  Header,
  Button,
} from 'semantic-ui-react';
import './index.css';
import { connect } from 'react-redux';
import AdminSideBar from '../../components/AdminSidebar';
import { ping } from '../../actions/pingpong';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSideBar: true,
    };
  }

  toggleSideBar = () => {
    const { showSideBar } = this.state;
    this.setState({
      showSideBar: !showSideBar,
    });
  };

  clickBtn = () => {
    this.props.ping();
  };

  render() {
    const { showSideBar } = this.state;


    return (
      <Sidebar.Pushable as="div">
        <AdminSideBar showSideBar={showSideBar} />
        <Sidebar.Pusher>
          <div className={showSideBar ? 'main-wrap nav-slide' : 'main-wrap'}>
            <Menu attached="top" secondary>
              <Menu.Item icon="bars" onClick={this.toggleSideBar} />
              <Menu.Item name="messages" onClick={this.handleItemClick} />
              <Menu.Item name="friends" onClick={this.handleItemClick} />
              <Dropdown item icon="wrench">
                <Dropdown.Menu>
                  <Dropdown.Item>
                    <Icon name="dropdown" />
                    <span className="text">New</span>

                    <Dropdown.Menu>
                      <Dropdown.Item>Document</Dropdown.Item>
                      <Dropdown.Item>Image</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown.Item>
                  <Dropdown.Item>Open</Dropdown.Item>
                  <Dropdown.Item>Save...</Dropdown.Item>
                  <Dropdown.Item>Edit Permissions</Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Header>Export</Dropdown.Header>
                  <Dropdown.Item>Share</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
            <Grid columns={1} padded>
              <Grid.Column>
                <Segment.Group>
                  <Segment>
                    <Header as="h5">Header</Header>
                  </Segment>
                  <Segment>
                    <Table celled>
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>
                            <Button primary onClick={this.clickBtn}>PING</Button>
                          </Table.HeaderCell>
                          <Table.HeaderCell>{this.props.pingpong === true ? 'true' : 'false'}</Table.HeaderCell>
                          <Table.HeaderCell>Header</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        <Table.Row>
                          <Table.Cell>First</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Cell</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                          <Table.Cell>Cell</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                          <Table.Cell>Cell</Table.Cell>
                        </Table.Row>
                      </Table.Body>

                      <Table.Footer>
                        <Table.Row>
                          <Table.HeaderCell colSpan="3">
                            <Menu floated="right" pagination>
                              <Menu.Item as="a" icon>
                                <Icon name="chevron left" />
                              </Menu.Item>
                              <Menu.Item as="a">1</Menu.Item>
                              <Menu.Item as="a">2</Menu.Item>
                              <Menu.Item as="a">3</Menu.Item>
                              <Menu.Item as="a">4</Menu.Item>
                              <Menu.Item as="a" icon>
                                <Icon name="chevron right" />
                              </Menu.Item>
                            </Menu>
                          </Table.HeaderCell>
                        </Table.Row>
                      </Table.Footer>
                    </Table>
                  </Segment>
                </Segment.Group>
              </Grid.Column>
            </Grid>
          </div>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}
function mapStateToProps(state) {
  return {
    auth: state.auth,
    pingpong: state.pingpong,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ping: () => dispatch(ping()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Admin);
