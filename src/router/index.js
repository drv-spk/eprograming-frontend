import React, { PureComponent } from 'react';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import AuthorizedRoute from '../components/Authorized';
import { routes } from '../configs';

const AppRoutes = props =>
  props.routes.map((route, index) => {
    if (route.redirect) {
      return (
        <AuthorizedRoute
          key={index}
          component={route.component}
          authority={route.authority !== false}
          path={route.path}
          exact={!!route.exact}
        />
      );
    }
    return <Redirect path={route.path} to={route.to} />;
  });

class AppRouter extends PureComponent {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <AppRoutes routes={routes} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default AppRouter;
