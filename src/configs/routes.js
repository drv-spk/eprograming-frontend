import AdminLogin from '../pages/AdminLogin';
import Admin from '../pages/Admin';
import ErrorPage403 from '../pages/Error/403';
import ErrorPage404 from '../pages/Error/404';
import ErrorPage500 from '../pages/Error/500';

export default [
  {
    path: '/admin/login',
    component: AdminLogin,
    authority: false,
    exact: true,
  },
  {
    path: '/admin',
    component: Admin,
    authority: false,
    exact: true,
  },
  {
    path: '/403',
    component: ErrorPage403,
    authority: false,
  },
  {
    path: '/404',
    component: ErrorPage404,
    authority: false,
  },
  {
    path: '/500',
    component: ErrorPage500,
    authority: false,
  },
];
