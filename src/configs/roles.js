const ROLE_STUDENT = 'ROLE_STUDENT';
const ROLE_TEACHER = 'ROLE_TEACHER';
const ROLE_ADMIN = 'ROLE_ADMIN';

export default { ROLE_STUDENT, ROLE_TEACHER, ROLE_ADMIN };
