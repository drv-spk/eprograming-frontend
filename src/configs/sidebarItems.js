export default [
  {
    title: 'Dashboard',
    icon: 'dashboard',
    url: '/admin/dashboard',
  },
  {
    title: 'Student',
    icon: 'group',
    url: '/admin/student',
  },
  {
    title: 'Resource',
    icon: 'folder open',
    url: '/admin/resource',
  },
];
