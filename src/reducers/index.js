import { combineReducers } from 'redux';
import auth from './authentication';
import pingpong from './pingpong';

export default combineReducers({
  auth,
  pingpong,
});
