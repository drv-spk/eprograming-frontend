import update from 'immutability-helper';
import { authentication } from '../actions/types';
import ApiStatus from '../utils/apiStatus';

const initialState = {
  data: {
    accessToken: null,
    info: null,
    isLoggedIn: false,
    role: null,
  },
  login: {
    status: ApiStatus.default(),
  },
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case authentication.LOGIN_REQUEST:
      return update(state, {
        login: { $set: ApiStatus.fetching() },
      });
    case authentication.LOGIN_FAILURE: {
      return update(state, {
        login: {
          status: { $set: ApiStatus.failure() },
        },
      });
    }
    case authentication.LOGIN_SUCCESS: {
      const { result } = action.payload;
      return update(state, {
        login: {
          status: { $set: ApiStatus.success() },
        },
        data: {
          accessToken: { $set: result.token },
          isLoggedIn: { $set: true },
        },
      });
    }
    case authentication.SET_USER_INFO_SUCCESS: {
      const { userInfo } = action.payload;
      return update(state, {
        data: {
          info: { $set: userInfo },
        },
      });
    }
    case authentication.USER_LOGOUT: {
      return update(state, {
        data: {
          accessToken: { $set: null },
          isLoggedIn: { $set: false },
        },
      });
    }
    default:
      return state;
  }
}
