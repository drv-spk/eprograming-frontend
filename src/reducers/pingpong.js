import { pingpong } from '../actions/types';

export default function pingpongReducer(state = false, action) {
  switch (action.type) {
    case pingpong.PING:
      return true;
    case pingpong.PONG:
      return false;
    default:
      return state;
  }
}
